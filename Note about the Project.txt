These are some things to know about the project:

1) It keeps track of the same time as the machine the program is currently running on.

2) The user can set three fields for an alarm
	-Hour (12 hour format)
	-Minute
	-Meridain Designation (AM/PM)

3) With the alarm time set, the user can activate the alarm when with the alarm button.

4) When the alarm time matches all the fields of the current time, the alarm will activate

5) The user can then choose to either turn of the alarm or snooze the alarm for 5 minutes

6) note: snooze will only turn of the alarm temporarilly and will continue to alarm until the alarm button has been switched off
