
#LabVIEW_Example:
This project is a practice workspace to learn the mechanics of git version control and bitbucket, particularly for controlling LabVIEW projects at Stimwave. Taking the time to understand the controls and power of git version control is a key aspect to having a development program that is robust, reliable and consitent. The LVAlarmClock.proj and ancillary .vi's were compiled in the directory to conduct the exercise. Changes were deliberately made to the file and its components to better understand workflow and how to interact with local and remote repositories.

##Background:
This Project includes the *Alarm Clock Application.vi* that receives inputs from *Actual Time.vi* and *Time Select.vi*. This project is designed to emulate the familiar functionality of common digital alarm clocks. The *Alarm Clock Application.vi* is an interface for a user to see current time as interpreted by the machine that .vi is running on. It also allows a user to set the **Hour (12 hour format) , Minute, and Meridan Designation (AM/PM)** of a desired alarm. Once the time is set to the desired setting and the **Alarm button** is activated, the alarm will trigger when the actual time of the of the .vi equals the time set by the user under **Alarm Set**. Once the alarm has been triggered, the user can then choose to **Snooze** for five minute increments or deactivate the **Alarm button**. It is important to note that the program will continue to alarm after the **Alarm Set** time is reachced, even if the **Snooze** feature is utilized, and will only stop when the **Alarm Button** has been deactivated. Deactivating the alarm rests the alram status and is able to be used again by the user as was done initially.

##Getting Started:
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

##Prerequisites:
You must have LabVIEW 2017 Student or greater to interact with this project.

##Installation:
1. Gain access to the repository from the admin with the appropriate access level for your purpose.
1. Clone the remote repository down to your local repository to access on your machine.
1. Simply navigate to the repository that your project is located and open the project.

For additional information on the steps above or to learn how to use git effectively, please refer to the **git guide** to ensure you are performing the steps correctly.

##Versioning:
Versioning is handled through BitBucket

##Authors and Contributors:
Ryan Zelaya - Originator of LVAlarmClock.proj and initiator of LabVIEW Example repo - Stimwave - Product Development/R&D

##Acknowledgments:
N/A