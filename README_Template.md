#Introduction:
*README* files are typically a starting point for someone viewing a project or repository for the first time. They are a simple way to orient new users on the details and scope of a project, as well as being a useful reference for seasoned users that are jumping from project to project. 
Information commonly compiled in README's include:

* What the project does
* Why the project is useful
* How users can get started with the project
* Where users can get help with your project
* Who maintains and contributes to the project

*Be sure to add any references and images to your descriptions/instructions as necessary*

#Sections to include:

##Project Title:
One paragraph of project description will be included here

##Getting Started:
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See Deployment for notes on how to deploy the project on a live system.

##Prerequisites:
Do your application require any additional tools or software to run?

**ex.**
Instacal
Runtime engine

##Installation:
Give step by step instructions to inform the user on how to get the application running

##Versioning:
Versioning is handled through BitBucket

##Authors and Contributors:
Author - Description of work - Company/department

##Acknowledgments:
Identify who's code was used for the project
Code used for inspiration
